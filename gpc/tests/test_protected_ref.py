"""
test_update protected branch/tag
----------------------------------
"""

# pylint: disable=unused-import

# Third Party Libraries
import pytest

from dictns import Namespace
from gitlab.v4.objects import Project

# Gitlab-Project-Configurator Modules
from gpc.executors.protected_branch_setting_executor import ProtectedBranchSettingExecutor
from gpc.helpers.exceptions import GpcUserError
from gpc.parameters import GpcParameters
from gpc.parameters import RunMode
from gpc.project_rule_executor import ProjectRuleExecutor
from gpc.tests.test_helpers import get_change_value
from gpc.tests.test_helpers import get_executor


# pylint: disable=redefined-outer-name, unused-argument, protected-access


def side_effet_user(username, retry_transient_errors):
    if username == "user.toto":
        return [Namespace({"id": 1234, "name": "user.toto", "username": "user.toto"})]
    raise GpcUserError("ERROR")


# flake8: noqa
@pytest.mark.parametrize(
    "project_rules, members_users, group_projects_list, exception_msg",
    [
        # 1 execution
        # project_rules
        # Rule to apply for the project
        (
            Namespace(
                {
                    "protected_branches": [
                        {
                            "pattern": "master",
                            "allowed_to_merge": "maintainers",
                            "allowed_to_push": {
                                "role": "kurt cobain",
                                "users": ["user.toto"],
                            },
                        },
                        {
                            "pattern": "dev*",
                            "allowed_to_merge": {"role": "developers"},
                            "allowed_to_push": {"role": "no one"},
                        },
                    ]
                }
            ),
            # members_users
            # Users which are members of the project
            [Namespace({"id": 1234, "name": "user.toto", "username": "user.toto"})],
            # group_projects_list
            # A list of projects which groups can access
            [
                {
                    "group_full_path": "test.group",
                    "group_id": 13,
                    "group_access_level": 666,
                }
            ],
            # exception_msg
            # Part of the exception message expected
            "The role 'kurt cobain' is not acceptable",
        ),
        # 2 execution
        # project_rules
        # Rule to apply for the project
        (
            Namespace(
                {
                    "member_profiles": [
                        Namespace({"name": "useless_profiles", "members": ["test/group/2"]}),
                        Namespace({"name": "mytest_profiles", "members": ["test/group"]}),
                    ],
                    "protected_branches": [
                        {
                            "pattern": "master",
                            "allowed_to_merge": "maintainers",
                            "allowed_to_push": {
                                "members": ["user.toto"],
                                "profiles": ["mytest_profiles"],
                            },
                        }
                    ],
                }
            ),
            # members_users
            # Users which are members of the project
            [Namespace({"id": 6859, "name": "member.user", "username": "member.user"})],
            # group_projects_list
            # A list of projects which groups can access
            [
                {
                    "group_full_path": "test.group",
                    "group_id": 13,
                    "group_access_level": 666,
                }
            ],
            # exception_msg
            # Part of the exception message expected
            str(["user.toto", "test/group"]),
        ),
    ],
)
# flake8: qa
def test_invalid_allow(
    mocker,
    fake_gitlab,
    fake_project,
    project_rules,
    members_users,
    group_projects_list,
    exception_msg,
):
    # Mock
    mocker.patch("gpc.tests.test_protected_ref.Project.save")
    mocker.patch(
        "gpc.tests.test_protected_ref.ProjectRuleExecutor.project",
        mocker.PropertyMock(return_value=fake_project),
    )
    users_mock = mocker.Mock()
    users_mock.list = mocker.Mock(side_effect=side_effet_user)
    fake_gitlab.users = users_mock

    groups_mock = get_groups_service_mock(mocker)
    fake_gitlab.groups = groups_mock
    fake_project.shared_with_groups = group_projects_list

    protectedbranches = mocker.Mock()

    protectedbranches.list = mocker.Mock(
        return_value=[
            Namespace(
                {
                    "name": "master",
                    "merge_access_levels": [{"access_level": 40}],
                    "push_access_levels": [{"access_level": 0}, {"user_id": 1234}],
                }
            )
        ]
    )

    fake_project.protectedbranches = protectedbranches

    members_all = mocker.Mock(return_value=members_users)
    mock_members = mocker.Mock()
    mock_members.all = members_all
    fake_project.members = mock_members

    p = ProjectRuleExecutor(
        gl=fake_gitlab,
        project_path="fake/path/to/project",
        rule=project_rules,
        gpc_params=GpcParameters(config=mocker.Mock("fake_config"), mode=RunMode.APPLY),
    )
    p.execute()
    executor = get_executor(p, ProtectedBranchSettingExecutor)
    assert exception_msg in executor.error_message


def get_groups_service_mock(mocker):
    group_mocked = mocker.Mock()
    group_mocked.id = 666
    group_mocked.name = "test.group"
    group_mocked.full_path = "test/group"
    members_all = mocker.Mock(
        return_value=[Namespace({"id": 1664, "name": "user.titi", "username": "user.titi"})]
    )
    mock_members = mocker.Mock()
    mock_members.all = members_all
    group_mocked.members = mock_members
    group_projects = mocker.Mock()
    group_mocked.projects = group_projects
    groups_mock = mocker.Mock()
    groups_mock.get = mocker.Mock(return_value=group_mocked)
    return groups_mock


@pytest.mark.parametrize("keep_variables", [True, False])
def test_create_protected_branch(mocker, fake_gitlab, fake_project, keep_variables):
    # Mock
    mocker.patch("gpc.tests.test_protected_ref.Project.save")
    mocker.patch(
        "gpc.tests.test_protected_ref.ProjectRuleExecutor.project",
        mocker.PropertyMock(return_value=fake_project),
    )
    mock_manager_branch = mocker.patch(
        "gitlab.v4.objects.ProjectProtectedBranchManager.create", mocker.Mock()
    )
    users_mock = mocker.Mock()
    users_mock.list = mocker.Mock(side_effect=side_effet_user)
    fake_gitlab.users = users_mock
    fake_gitlab.groups = get_groups_service_mock(mocker)
    fake_project.shared_with_groups = [
        {"group_full_path": "test/group", "group_id": 66, "group_access_level": 30}
    ]

    members_all = mocker.Mock(
        return_value=[Namespace({"id": 1234, "name": "user.toto", "username": "user.toto"})]
    )
    mock_members = mocker.Mock()
    mock_members.all = members_all
    fake_project.members = mock_members

    protectedbranches = mocker.Mock()

    protectedbranches.list = mocker.Mock(
        return_value=[
            Namespace(
                {
                    "name": "master",
                    "merge_access_levels": [{"access_level": 40}],
                    "push_access_levels": [{"access_level": 0}, {"user_id": 1234}],
                    "allow_force_push": False,
                    "code_owner_approval_required": False,
                }
            ),
            Namespace(
                {
                    "name": "other",
                    "merge_access_levels": [{"access_level": 40}],
                    "push_access_levels": [{"access_level": 0}, {"user_id": 1234}],
                    "allow_force_push": False,
                    "code_owner_approval_required": False,
                }
            ),
        ]
    )
    fake_project.protectedbranches = protectedbranches

    project_rules = Namespace(
        {
            "member_profiles": [
                Namespace({"name": "useless_profiles", "members": ["test/group/2"]}),
                Namespace({"name": "mytest_profiles", "members": ["test/group"]}),
            ],
            "keep_existing_protected_branches": keep_variables,
            "protected_branches": [
                {
                    "pattern": "master",
                    "allowed_to_merge": "maintainers",
                    "allowed_to_push": {"role": "no one", "members": ["user.toto"]},
                    "allow_force_push": True,
                    "code_owner_approval_required": True,
                },
                {
                    "pattern": "dev*",
                    "allowed_to_merge": {
                        "members": ["user.toto"],
                        "profiles": ["mytest_profiles"],
                    },
                    "allowed_to_push": {"role": "no one"},
                },
            ],
        }
    )

    p = ProjectRuleExecutor(
        gl=fake_gitlab,
        project_path="fake/path/to/project",
        rule=project_rules,
        gpc_params=GpcParameters(config=mocker.Mock("fake_config"), mode=RunMode.APPLY),
    )
    p.execute()
    change_str = p._changes_to_string()
    change_protected_branches = get_change_value(p, "protected_branches")

    assert len(change_protected_branches.differences) == 3
    assert change_protected_branches.differences.get("master").get("status") == "updated"
    assert change_protected_branches.differences.get("dev*").get("status") == "added"

    if keep_variables:
        assert change_protected_branches.differences.get("other").get("status") == "kept"
    else:
        assert change_protected_branches.differences.get("other").get("status") == "removed"

    assert len(change_protected_branches.after) == 2
    assert change_protected_branches.after[0].name == "master"
    assert change_protected_branches.after[0].allowed_to_push.role.member_id == 0
    assert change_protected_branches.after[0].allowed_to_merge.role.member_id == 40
    assert change_protected_branches.differences.get("master").get("status") == "updated"
    assert change_protected_branches.after[0].allowed_to_push.users[0].member_id == 1234
    assert change_protected_branches.after[0].allowed_to_push.users[0].name == "user.toto"
    assert change_protected_branches.after[1].name == "dev*"
    assert change_protected_branches.after[1].allowed_to_push.role.member_id == 0
    assert change_protected_branches.after[1].allowed_to_merge.groups[0].name == "test/group"
    assert not change_protected_branches.after[1].allowed_to_merge.role
    assert change_protected_branches.differences.get("dev*").get("status") == "added"
    assert "maintainers" in change_str
    assert mock_manager_branch.is_called


@pytest.mark.parametrize("keep_variables", [True, False])
def test_create_protected_tag(mocker, fake_gitlab, fake_project, keep_variables):
    # Mock
    mocker.patch("gpc.tests.test_protected_ref.Project.save")
    mocker.patch(
        "gpc.tests.test_protected_ref.ProjectRuleExecutor.project",
        mocker.PropertyMock(return_value=fake_project),
    )
    mock_manager_tag = mocker.patch(
        "gitlab.v4.objects.ProjectProtectedTagManager.create", mocker.Mock()
    )
    protectedtags = mocker.Mock()

    protectedtags.list = mocker.Mock(
        return_value=[
            Namespace({"name": "master", "create_access_levels": [{"access_level": 40}]}),
            Namespace({"name": "tag1", "create_access_levels": [{"access_level": 40}]}),
        ]
    )

    fake_project.protectedtags = protectedtags

    project_rules = Namespace(
        {
            "keep_existing_protected_tags": keep_variables,
            "protected_tags": [
                {"pattern": "master", "allowed_to_create": "maintainers"},
                {"pattern": "dev*", "allowed_to_create": "developers"},
            ],
        }
    )

    p = ProjectRuleExecutor(
        gl=fake_gitlab,
        project_path="fake/path/to/project",
        rule=project_rules,
        gpc_params=GpcParameters(config=mocker.Mock("fake_config"), mode=RunMode.APPLY),
    )
    p.execute()
    change_protected_tags = get_change_value(p, "protected_tags")
    change_str = p._changes_to_string()
    assert len(change_protected_tags.after) == 2
    assert change_protected_tags.after[0].name == "master"
    assert change_protected_tags.after[0].allowed_to_create == [40]
    assert change_protected_tags.differences.get("master").get("status") == "kept"
    assert change_protected_tags.after[1].name == "dev*"
    assert change_protected_tags.after[1].allowed_to_create == [30]
    assert change_protected_tags.differences.get("dev*").get("status") == "added"
    if keep_variables:
        assert change_protected_tags.differences.get("tag1").get("status") == "kept"
    else:
        assert change_protected_tags.differences.get("tag1").get("status") == "removed"
    assert mock_manager_tag.is_called
    assert "maintainers" in change_str
