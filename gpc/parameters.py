"""
Command line parameters store.
"""

# Standard Library
from enum import Enum
from typing import Any  # pylint: disable=unused-import
from typing import List  # pylint: disable=unused-import
from typing import Optional  # pylint: disable=unused-import
from typing import Union  # pylint: disable=unused-import

# Third Party Libraries
import attr

from dictns import Namespace
from path import Path  # pylint: disable=unused-import


RawConfig = Namespace


class RunMode(Enum):

    DRY_RUN = "dry-run"
    APPLY = "apply"
    INTERACTIVE = "interactive"

    def __str__(self) -> str:
        return str(self.value)


@attr.s
class GpcParameters:
    config: Path = attr.ib()
    force: List[str] = attr.ib(default=[])
    mode: RunMode = attr.ib(default=RunMode.DRY_RUN)
    projects: List[str] = attr.ib(default=None)
    report_file: str = attr.ib(default=None)
    report_html: str = attr.ib(default=None)
    diff: bool = attr.ib(default=False)
    debug: bool = attr.ib(default=False)
    config_project_url: str = attr.ib(default=None)
    gpc_enabled_badge_url: str = attr.ib(default=None)
    gpc_accepted_external_badge_image_urls: str = attr.ib(default=None)
    smtp_server: str = attr.ib(default=None)
    smtp_port: str = attr.ib(default=None)
    email_author: str = attr.ib(default=None)
    watchers: List[str] = attr.ib(default=None)
