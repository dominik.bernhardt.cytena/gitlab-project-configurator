"""
Make the approval rules for Gitlab Premium.
"""
# Standard Library
from typing import Dict  # pylint: disable=unused-import

# Third Party Libraries
import attr
import click

from dictns import Namespace
from structlog import get_logger

# Gitlab-Project-Configurator Modules
from gpc.executors.approval_settings_executor import ChangeApprovers
from gpc.executors.profile_member_mixin import GPCUser
from gpc.executors.profile_member_mixin import ProfileMemberMixin
from gpc.executors.properties_updator import ChangePropertyExecutor
from gpc.helpers.exceptions import GpcMemberError
from gpc.helpers.project_approval import ProjectApprovalRules
from gpc.parameters import RunMode
from gpc.property_manager import PropertyBean


log = get_logger()


@attr.s
class ProjectApproversRules(PropertyBean):

    name = attr.ib(default="")  # type: str
    users = attr.ib(default=None)  # type: dict
    groups = attr.ib(default=None)  # type: dict
    approvals_before_merge = attr.ib(default=0)  # type: int
    protected_branches = attr.ib(default=list)  # type: list

    @property
    def remove_members(self):
        return (
            self.users is not None
            and self.groups is not None
            and (len(self.users) + len(self.groups)) == 0
        )

    @staticmethod
    def to_project_approvers(api_approvers, rule_name):
        project_approvers = ProjectApproversRules()
        if rule_name in api_approvers.rule_names:
            project_approvers.name = rule_name
            project_approvers.protected_branches = api_approvers.protected_branches_per_rule.get(
                rule_name, []
            )
            project_approvers.approvals_before_merge = (
                api_approvers.approvals_before_merge_per_rule.get(rule_name, 0)
            )
            groups = {}
            if api_approvers.approver_groups_per_rule.get(rule_name):
                for group in api_approvers.approver_groups_per_rule.get(rule_name):
                    group_id = group.get("id")
                    group_name = group.get("name")
                    full_path = group.get("full_path")
                    groups[group_id] = ApproverGroup(group_id, group_name, full_path)
            project_approvers.groups = groups
            users = {}

            if api_approvers.approvers_per_rule.get(rule_name):
                for user in api_approvers.approvers_per_rule.get(rule_name):
                    user_id = user.get("id")
                    user_name = user.get("username")
                    users[user_id] = ApproverUser(user_id, user_name)
            project_approvers.users = users
        return project_approvers

    def get_query(self):
        pass

    def to_dict(self):
        dict_variable = {
            "name": self.name,
            "approvals_before_merge": self.approvals_before_merge,
            "protected_branches": self.protected_branches,
        }
        if self.users:
            dict_variable["users"] = [user.name for user in self.users.values()]
        if self.groups:
            dict_variable["groups"] = [group.full_path for group in self.groups.values()]
        return dict_variable

    def get_user_ids(self):
        if self.users:
            return list(self.users.keys())
        return None

    def get_group_ids(self):
        if self.groups:
            return list(self.groups.keys())
        return None


@attr.s
class ApproverUser:

    user_id = attr.ib()  # type: int
    name = attr.ib()  # type: str

    def __str__(self):
        return self.name


@attr.s
class ApproverGroup:

    group_id = attr.ib()  # type: int
    name = attr.ib()  # type: str
    full_path = attr.ib()  # type: str

    def __str__(self):
        return self.full_path


class ApprovalRulesExecutor(ChangePropertyExecutor, ProfileMemberMixin):

    order = 55
    sections = ["approval_rules"]

    def _apply(self):  # noqa: C901

        if not self.changes:
            return
        # We delete the rules that are not in common_policies.yml
        project_rules = self.project.approvalrules.list(all=True)
        conf_rules = [rule["name"] for rule in self.rule.approval_rules]
        for rule in project_rules:
            if rule.attributes["name"] not in conf_rules:
                self.project.approvalrules.delete(
                    id=rule.attributes["id"], retry_transient_errors=True
                )

        user_errors = []
        for change in self.changes:

            if change.action == "kept":
                continue

            approvals_rule = change
            manager = ProjectApprovalRules(self.project)
            approvers_to_change = approvals_rule.after  # type: ProjectApprovalRules
            protected_branches = self.compute_branches(approvers_to_change.protected_branches)

            if not protected_branches and approvers_to_change.protected_branches:
                # If all the given protected branches are invalid (absent from Gitlab) we won't
                # create an approval rule
                warning = (
                    f'Approval rule "{approvers_to_change.name}" was not applied because none '
                    "of the given protected branches were present in Gitlab"
                )
                click.secho(warning, fg="yellow")
                continue
            if len(protected_branches) < len(approvers_to_change.protected_branches):
                # If some protected branches are invalid, we create an approval rule
                # just for the valid ones
                warning = (
                    f'Approval rule "{approvers_to_change.name}" partially applied '
                    "(some of the given protected branches were not present on your project)"
                )
                click.secho(warning, fg="yellow")

            query = {
                "approvals_required": approvers_to_change.approvals_before_merge,
                "name": approvers_to_change.name,
                "premium": True,
            }

            if protected_branches:
                query["protected_branches"] = protected_branches
            if not approvers_to_change.remove_members:
                if approvers_to_change.users:
                    query["approver_ids"] = approvers_to_change.get_user_ids()
                if approvers_to_change.groups:
                    query["approver_group_ids"] = approvers_to_change.get_group_ids()

            manager.set_approvers(**query)
            if approvers_to_change.users:
                user_errors += self._check_approvers(approvers_to_change, manager)

        self._approver_raise_user_errors(user_errors)

    def _check_approvers(self, approvers_to_change, manager):
        user_ids = approvers_to_change.get_user_ids()
        remote_approvers = [a["id"] for a in manager.approvers]
        user_ids_error = list(set(user_ids) - set(remote_approvers))
        user_errors = [approvers_to_change.users[user_id].name for user_id in user_ids_error]
        return user_errors

    def _approver_raise_user_errors(self, user_errors=None):
        if user_errors:
            raise GpcMemberError(
                f"For the project '{self.project_path}', "
                f"these users can not be added as "
                f"approvers: {', '.join(user_errors)}.\n"
                f"Please check that these users are members "
                f"of the project, or the parent project."
            )

    def _update(self, mode: RunMode, members_user, members_group):
        if "approval_rules" not in self.rule or self.rule.approval_rules is None:
            return
        project_approval = ProjectApprovalRules(self.project)  # type: ignore

        for rule in self.rule.approval_rules:

            old_approvers = ProjectApproversRules.to_project_approvers(project_approval, rule.name)
            project_approvers = self.to_project_approvers(old_approvers, rule)
            self.changes.append(
                ChangeApprovers(
                    "approval_rules", old_approvers, project_approvers, self.show_diff_only
                )
            )

    def to_project_approvers(self, old_approvers, rule):
        # If field is None in config, we set the value of current config.
        # To display we keep the value
        approvers = self._prepare_approvers(rule)
        project_approvers = ProjectApproversRules()
        project_approvers.name = rule.name
        project_approvers.protected_branches = rule.get("protected_branches", [])

        project_approvers.approvals_before_merge = approvers.get(
            "minimum", old_approvers.approvals_before_merge
        )
        if approvers.get("members") is not None:
            users = {}
            groups = {}
            for member_name in approvers.get("members"):
                self._update_approvers(member_name, users, groups)
            project_approvers.users = users
            project_approvers.groups = groups
        else:
            project_approvers.users = old_approvers.users
            project_approvers.groups = old_approvers.groups
        return project_approvers

    def _prepare_approvers(self, ns_approvers: Namespace) -> Dict:
        approvers = ns_approvers.copy()
        if "profiles" in approvers:
            merged_profiles = self.get_merged_profiles(approvers.get("profiles"))
            members = approvers.get("members", [])
            approvers["members"] = list(set(merged_profiles + members))
            del approvers["profiles"]
        return approvers

    def _update_approvers(self, name, users_approver, groups_approver):
        member = self._find_member(name)
        if isinstance(member, GPCUser):
            users_approver[member.gl_id] = ApproverUser(member.gl_id, name)
        else:
            # GPCGroup
            groups_approver[member.gl_id] = ApproverGroup(
                member.gl_id, member.name, member.full_path
            )

    def compute_branches(self, protected_branches):
        """Compute protected branches ids.

        This function will keep protected branches ids if given and transform an existing
        branch name into an id if a string is given

        Returns
        -------
        Protected branches ids for the merge request approval rule we want to create
        """

        pb = []
        if not protected_branches:
            return pb
        remote_pbs = self.project.protectedbranches.list(
            as_list=True, all=True, retry_transient_errors=True
        )
        for branch in protected_branches:
            if isinstance(branch, int):
                pb.append(branch)
            if isinstance(branch, str):
                is_warning = True
                for b in remote_pbs:
                    if b.attributes["name"] == branch:
                        is_warning = False
                        pb.append(b.attributes["id"])
                        break
                if is_warning:
                    warning = (
                        f"{branch} is not a protected branch of {self.project.path_with_namespace},"
                        " please make sure to add it to your GPC schema or to your project"
                    )
                    click.secho(warning, fg="yellow")
        return pb
