"""
Make the approval setting.
"""
# Standard Library
from typing import Dict  # pylint: disable=unused-import

# Third Party Libraries
import attr

from dictns import Namespace
from structlog import get_logger

# Gitlab-Project-Configurator Modules
from gpc.change_setting import ChangeNamedPropertyMixin
from gpc.change_setting import ChangeSetting
from gpc.executors.profile_member_mixin import GPCUser
from gpc.executors.profile_member_mixin import ProfileMemberMixin
from gpc.executors.properties_updator import ChangePropertyExecutor
from gpc.helpers.exceptions import GpcMemberError
from gpc.helpers.project_approval import ProjectApproval
from gpc.parameters import RunMode
from gpc.property_manager import PropertyBean


log = get_logger()


@attr.s
class ProjectApprovers(PropertyBean):

    name = attr.ib(default="approvers")  # type: str
    users = attr.ib(default=None)  # type: dict
    groups = attr.ib(default=None)  # type: dict
    approvals_before_merge = attr.ib(default=None)  # type: int
    reset_approvals_on_push = attr.ib(default=None)  # type: bool
    can_override_approvals_per_merge_request = attr.ib(default=None)  # type: bool
    enable_self_approval = attr.ib(default=None)  # type: bool
    enable_committers_approvers = attr.ib(default=None)  # type: bool

    @property
    def disable_overriding_approvers_per_merge_request(self):
        return not self.can_override_approvals_per_merge_request

    @property
    def remove_members(self):
        return (
            self.users is not None
            and self.groups is not None
            and (len(self.users) + len(self.groups)) == 0
        )

    @staticmethod
    def to_project_approvers(api_approvers):
        project_approvers = ProjectApprovers()
        project_approvers.approvals_before_merge = api_approvers.approvals_before_merge
        project_approvers.reset_approvals_on_push = api_approvers.reset_approvals_on_push
        project_approvers.enable_self_approval = api_approvers.merge_requests_author_approval
        project_approvers.enable_committers_approvers = api_approvers.enable_committers_approvers
        project_approvers.can_override_approvals_per_merge_request = (
            not api_approvers.disable_overriding_approvers_per_merge_request
        )
        groups = {}
        if api_approvers.approver_groups:
            for group in api_approvers.approver_groups:
                group_id = group.get("id")
                name = group.get("name")
                full_path = group.get("full_path")
                groups[group_id] = ApproverGroup(group_id, name, full_path)
        project_approvers.groups = groups
        users = {}
        if api_approvers.approvers:
            for user in api_approvers.approvers:
                user_id = user.get("id")
                name = user.get("username")
                users[user_id] = ApproverUser(user_id, name)
        project_approvers.users = users
        return project_approvers

    def get_query(self):
        pass

    def to_dict(self):
        can_override = self.can_override_approvals_per_merge_request
        dict_variable = {
            "name": self.name,
            "approvals_before_merge": self.approvals_before_merge,
            "reset_approvals_on_push": self.reset_approvals_on_push,
            "can_override_approvals_per_merge_request": can_override,
            "enable_self_approval": self.enable_self_approval,
            "enable_committers_approvers": self.enable_committers_approvers,
        }
        if self.users:
            dict_variable["users"] = [user.name for user in self.users.values()]
        if self.groups:
            dict_variable["groups"] = [group.full_path for group in self.groups.values()]
        return dict_variable

    def get_user_ids(self):
        if self.users:
            return list(self.users.keys())
        return None

    def get_group_ids(self):
        if self.groups:
            return list(self.groups.keys())
        return None


@attr.s
class ApproverUser:

    user_id = attr.ib()  # type: int
    name = attr.ib()  # type: str

    def __str__(self):
        return self.name


@attr.s
class ApproverGroup:

    group_id = attr.ib()  # type: int
    name = attr.ib()  # type: str
    full_path = attr.ib()  # type: str

    def __str__(self):
        return self.full_path


@attr.s
class ChangeApprovers(ChangeSetting):
    REF_PROPERTY = ChangeNamedPropertyMixin.REF_PROPERTY

    def __str__(self):

        change_str = self.FMT_TITLE.format(
            property_name="{}{}".format("      " * self.sub_level, self.property_name),
            before="",
            after="",
            action=self.action,
        )

        after = self.after.to_dict()
        before = self.before.to_dict()
        for name, value in after.items():

            change_str += (
                self.REF_PROPERTY.format(
                    sub_prop="      {}{}".format("      " * self.sub_level, name),
                    before=str(before.get(name)),
                    after=str(value),
                )
                + "\n"
            )

        return change_str

    def to_dict(self):
        return {
            "property_name": self.property_name,
            "differences": {
                "before": self.before.to_dict(),
                "after": self.after.to_dict(),
                "action": self.action,
            },
        }


class ApprovalSettingExecutor(ChangePropertyExecutor, ProfileMemberMixin):

    order = 60
    sections = ["approvers"]

    def _apply(self):
        if self.changes:
            approvers = self.changes[0]
            if approvers.action == "kept":
                return
            manager = ProjectApproval(self.project)
            approvers_to_change = approvers.after  # type: ProjectApprovers
            manager.reset_approvals_on_push = approvers_to_change.reset_approvals_on_push
            manager.merge_requests_author_approval = approvers_to_change.enable_self_approval
            manager.enable_committers_approvers = approvers_to_change.enable_committers_approvers
            manager.disable_overriding_approvers_per_merge_request = (
                approvers_to_change.disable_overriding_approvers_per_merge_request
            )
            manager.save()
            query = {"approvals_required": approvers_to_change.approvals_before_merge}
            if not approvers_to_change.remove_members:
                if approvers_to_change.users:
                    query["approver_ids"] = approvers_to_change.get_user_ids()
                if approvers_to_change.groups:
                    query["approver_group_ids"] = approvers_to_change.get_group_ids()
            manager.set_approvers(**query)
            if approvers_to_change.users:
                self._check_approvers(approvers_to_change, manager)

    def _check_approvers(self, approvers_to_change, manager):
        user_ids = approvers_to_change.get_user_ids()
        remote_approvers = [a["id"] for a in manager.approvers]
        user_ids_error = list(set(user_ids) - set(remote_approvers))
        user_errors = [approvers_to_change.users[user_id].name for user_id in user_ids_error]
        if user_errors:
            raise GpcMemberError(
                f"For the project '{self.project_path}', "
                f"these users can not be added as "
                f"approvers: {', '.join(user_errors)}.\n"
                f"Please check that these users are members "
                f"of the project, or the parent project."
            )

    def _update(self, mode: RunMode, members_user, members_group):
        if "approvers" not in self.rule or self.rule.approvers is None:
            return
        approvers = self._prepare_approvers(self.rule.approvers)
        project_approval = ProjectApproval(self.project)  # type: ignore
        old_approvers = ProjectApprovers.to_project_approvers(project_approval)
        project_approvers = self.to_project_approvers(approvers, old_approvers)
        self.changes.append(
            ChangeApprovers("approvers", old_approvers, project_approvers, self.show_diff_only)
        )

    def to_project_approvers(self, approvers, old_approvers):
        # If field is None in config, we set the value of current config.
        # To display we keep the value
        project_approvers = ProjectApprovers()
        project_approvers.approvals_before_merge = approvers.get(
            "minimum", old_approvers.approvals_before_merge
        )
        ApprovalSettingExecutor.init_options(approvers, project_approvers, old_approvers)
        if approvers.get("members") is not None:
            users = {}
            groups = {}
            for member_name in approvers.get("members"):
                self._update_approvers(member_name, users, groups)
            project_approvers.users = users
            project_approvers.groups = groups
        else:
            project_approvers.users = old_approvers.users
            project_approvers.groups = old_approvers.groups
        return project_approvers

    @staticmethod
    def init_options(approvers, project_approvers, old_approvers):
        options = approvers.get("options")
        if options:
            project_approvers.reset_approvals_on_push = options.get(
                "remove_all_approvals_when_new_commits_are_pushed",
                old_approvers.reset_approvals_on_push,
            )
            project_approvers.can_override_approvals_per_merge_request = options.get(
                "can_override_approvals_per_merge_request",
                old_approvers.can_override_approvals_per_merge_request,
            )
            project_approvers.enable_self_approval = options.get(
                "enable_self_approval", old_approvers.enable_self_approval
            )
            project_approvers.enable_committers_approvers = options.get(
                "enable_committers_approvers", old_approvers.enable_committers_approvers
            )

        else:
            project_approvers.reset_approvals_on_push = old_approvers.reset_approvals_on_push
            project_approvers.enable_self_approval = old_approvers.enable_self_approval
            project_approvers.enable_committers_approvers = (
                old_approvers.enable_committers_approvers
            )
            project_approvers.can_override_approvals_per_merge_request = (
                old_approvers.can_override_approvals_per_merge_request
            )

    def _prepare_approvers(self, ns_approvers: Namespace) -> Dict:
        approvers = ns_approvers.copy()
        if "profiles" in approvers:
            merged_profiles = self.get_merged_profiles(approvers.get("profiles"))
            members = approvers.get("members", [])
            approvers["members"] = list(set(merged_profiles + members))
            del approvers["profiles"]
        return approvers

    def _update_approvers(self, name, users_approver, groups_approver):
        member = self._find_member(name)
        if isinstance(member, GPCUser):
            users_approver[member.gl_id] = ApproverUser(member.gl_id, name)
        else:
            # GPCGroup
            groups_approver[member.gl_id] = ApproverGroup(
                member.gl_id, member.name, member.full_path
            )
