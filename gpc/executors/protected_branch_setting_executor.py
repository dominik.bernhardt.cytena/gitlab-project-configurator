"""
Make the update of protected branch.
"""
# Standard Library
from typing import List  # pylint: disable=unused-import

# Third Party Libraries
import attr

from boltons.cacheutils import cachedproperty
from dictns import Namespace
from gitlab import exceptions as gl_exceptions
from structlog import get_logger

# Gitlab-Project-Configurator Modules
from gpc.change_setting import ChangePropertySetting
from gpc.executors.profile_member_mixin import GPCUser
from gpc.executors.profile_member_mixin import ProfileMemberMixin
from gpc.executors.properties_updator import ChangePropertyExecutor
from gpc.helpers.exceptions import GpcMemberError
from gpc.helpers.exceptions import GpcPermissionError
from gpc.helpers.gitlab_helper import MAP_ACCESS_REVERT
from gpc.helpers.gitlab_helper import get_group
from gpc.helpers.gitlab_helper import get_user_by_id
from gpc.parameters import RunMode
from gpc.property_manager import PropertyBean
from gpc.property_manager import PropertyManager


log = get_logger()


@attr.s(eq=False)
class ProtectedBranch(PropertyBean):

    allowed_to_merge = attr.ib()  # type: ProtectedRefsAuth
    allowed_to_push = attr.ib()  # type: ProtectedRefsAuth
    allow_force_push = attr.ib()
    code_owner_approval_required = attr.ib()

    @staticmethod
    def to_protected_branches(gitlab, api_protected_branches):
        protected_branches = []  # type: List[ProtectedBranch]
        for api_protected_branch in api_protected_branches:
            protected_branches.append(
                ProtectedBranch.to_protected_branch(gitlab, api_protected_branch)
            )
        return protected_branches

    @staticmethod
    def to_protected_branch(gitlab, api_protected_branch):
        merge_role, merge_users = ProtectedBranch.get_role_and_users(
            api_protected_branch.merge_access_levels, gitlab
        )

        push_role, push_users = ProtectedBranch.get_role_and_users(
            api_protected_branch.push_access_levels, gitlab
        )

        allowed_to_merge = ProtectedRefsAuth(role=merge_role, users=merge_users)
        allowed_to_push = ProtectedRefsAuth(role=push_role, users=push_users)
        allow_force_push = api_protected_branch.allow_force_push
        code_owner_approval_required = api_protected_branch.code_owner_approval_required

        return ProtectedBranch(
            name=api_protected_branch.name,
            allowed_to_merge=allowed_to_merge,
            allowed_to_push=allowed_to_push,
            code_owner_approval_required=code_owner_approval_required,
            allow_force_push=allow_force_push,
        )

    @staticmethod
    def get_role_and_users(access_levels, gitlab):
        users = []
        role = None
        for access in access_levels:
            if access.get("user_id") is not None:
                user_id = access.get("user_id")
                user = get_user_by_id(gitlab, user_id)
                users.append(ProtectedRefMember(user_id, user.username))
            else:  # get role
                role_id = access.get("access_level")
                role = ProtectedRefMember(role_id, MAP_ACCESS_REVERT.get(role_id))
        return role, users

    def get_query(self):
        allow_to_merge = ProtectedBranch.prepare_allow_action(self.allowed_to_merge)
        allow_to_push = ProtectedBranch.prepare_allow_action(self.allowed_to_push)
        obj = {
            "name": self.name,
            "allowed_to_push": allow_to_push,
            "allowed_to_merge": allow_to_merge,
        }
        if self.allow_force_push is not None:
            obj.update({"allow_force_push": self.allow_force_push})
        if self.code_owner_approval_required is not None:
            obj.update({"code_owner_approval_required": self.code_owner_approval_required})
        return obj

    @staticmethod
    def prepare_allow_action(allow_action):
        actions_list = []
        if allow_action.role:
            actions_list.append({"access_level": allow_action.role.member_id})
        if allow_action.users:
            for identifier in allow_action.users:
                actions_list.append({"user_id": identifier.member_id})
        if allow_action.groups:
            for identifier in allow_action.groups:
                actions_list.append({"group_id": identifier.member_id})
        return actions_list

    def to_dict(self):
        obj = {
            "name": self.name,
            "allowed_to_merge": self.allowed_to_merge.get_members_hr(),
            "allowed_to_push": self.allowed_to_push.get_members_hr(),
        }
        if self.allow_force_push is not None:
            obj.update({"allow_force_push": self.allow_force_push})
        if self.code_owner_approval_required is not None:
            obj.update({"code_owner_approval_required": self.code_owner_approval_required})
        return obj

    def __eq__(self, other):
        if not isinstance(other, ProtectedBranch):
            return False

        comp = (
            self.name == other.name
            and self.allowed_to_push == other.allowed_to_push
            and self.allowed_to_merge == other.allowed_to_merge
        )

        if self.allow_force_push is None or other.allow_force_push is None:  # ignore
            pass
        else:
            comp = comp and self.allow_force_push == other.allow_force_push

        if (
            self.code_owner_approval_required is None or other.code_owner_approval_required is None
        ):  # ignore
            pass
        else:
            comp = comp and self.code_owner_approval_required == other.code_owner_approval_required

        return comp


class ChangeProtectedBranch(ChangePropertySetting):

    sub_properties = [
        "allowed_to_merge",
        "allowed_to_push",
        "allow_force_push",
        "code_owner_approval_required",
    ]
    status_to_process = ["removed", "updated", "kept", "added"]


@attr.s(eq=False)
class ProtectedRefMember:
    member_id = attr.ib()  # type: int
    name = attr.ib()  # type: str

    def __eq__(self, other):
        if not isinstance(other, ProtectedRefMember):
            return False
        return self.member_id == other.member_id and self.name.replace(
            "none", "no one"
        ) == other.name.replace("none", "no one")


@attr.s(eq=False)
class ProtectedRefsAuth:
    role = attr.ib(default=None)  # type: ProtectedRefMember
    users = attr.ib(default=[])  # type: List[ProtectedRefMember]
    groups = attr.ib(default=[])  # type: List[ProtectedRefMember]
    code_owner_approval_required = attr.ib(default=None)
    allow_force_push = attr.ib(default=None)

    def sorted_users(self):
        return (
            sorted((c for c in self.users), key=lambda x: x.member_id)
            if self.users is not None
            else None
        )

    def sorted_groups(self):
        return (
            sorted((c for c in self.groups), key=lambda x: x.member_id)
            if self.groups is not None
            else None
        )

    def get_members_hr(self):
        roles_name = []
        users_name = []
        groups_name = []
        if self.role:
            roles_name = [MAP_ACCESS_REVERT.get(self.role.member_id)]
        if self.users:
            users_name = sorted(x.name for x in self.users)
        if self.groups:
            groups_name = sorted(x.name for x in self.groups)
        return roles_name + users_name + groups_name

    def __eq__(self, other):
        if not isinstance(other, ProtectedRefsAuth):
            return False

        return (
            self.role == other.role
            and self.sorted_users() == other.sorted_users()
            and self.sorted_groups() == other.sorted_groups()
            and self.allow_force_push == other.allow_force_push
            and self.code_owner_approval_required == other.code_owner_approval_required
        )


class ProtectedBranchSettingExecutor(ChangePropertyExecutor, ProfileMemberMixin):

    order = 20
    sections = ["protected_branches"]

    @cachedproperty
    def members_id(self):
        members = [
            x.id
            for x in self.project.members.all(all=True, as_list=False, retry_transient_errors=True)
        ]
        for group_path in self.members_group:
            group = get_group(self.gitlab, group_path)
            for member in group.members.all(all=True, as_list=False, retry_transient_errors=True):
                members.append(member.id)
        return list(set(members))

    @cachedproperty
    def members_group(self):
        return [x.get("group_full_path") for x in self.project.shared_with_groups]

    def _apply(self):
        if self.changes:
            protected_branches = self.changes[0]
            try:
                self._save_properties(
                    PropertyManager(self.project.protectedbranches),
                    protected_branches,
                    protected_branches.after,
                )
            except gl_exceptions.GitlabCreateError as e:
                if e.response_code == 422:
                    raise GpcPermissionError(
                        "Are you sure yours users or groups are members"
                        " of the project {} ?\nError: {}".format(self.project_path, str(e))
                    )

    def _update(self, mode: RunMode, members_user, members_group):
        if "protected_branches" in self.rule and self.rule.protected_branches is not None:
            protected_branches = []
            keep_existing_branches = self.rule.get("keep_existing_protected_branches", False)

            for protected_branch in self.rule.protected_branches:
                protected_branches.append(
                    self._to_protected_branch(protected_branch, members_user, members_group)
                )
            self.changes.append(
                ChangeProtectedBranch(
                    property_name="protected_branches",
                    before=ProtectedBranch.to_protected_branches(
                        self.gitlab,
                        self.project.protectedbranches.list(  # type: ignore
                            as_list=False, retry_transient_errors=True
                        ),
                    ),
                    after=protected_branches,
                    show_diff_only=self.show_diff_only,
                    keep_existing=keep_existing_branches,
                )
            )

    def _to_protected_branch(self, protected_branch, future_members_user, future_members_group):
        new_protected_branch = self.prepare_protected_branch(protected_branch)
        allowed_to_merge = self.init_protected_refs_auth(
            new_protected_branch.allowed_to_merge,
            future_members_user,
            future_members_group,
        )
        allowed_to_push = self.init_protected_refs_auth(
            new_protected_branch.allowed_to_push,
            future_members_user,
            future_members_group,
        )

        options = self.init_protected_branch_options(new_protected_branch)

        params = options.copy()
        params.update(
            {
                "name": new_protected_branch.pattern,
                "allowed_to_merge": allowed_to_merge,
                "allowed_to_push": allowed_to_push,
            }
        )

        return ProtectedBranch(**params)

    def init_protected_branch_options(self, protected_branch):
        opts = {"allow_force_push": None, "code_owner_approval_required": None}
        if "allow_force_push" in protected_branch:
            opts.update({"allow_force_push": protected_branch.allow_force_push})
        if "code_owner_approval_required" in protected_branch:
            opts.update(
                {"code_owner_approval_required": protected_branch.code_owner_approval_required}
            )

        return opts

    def prepare_protected_branch(self, protected_branch: Namespace):
        new_protected_branch = Namespace(protected_branch.copy())
        self._update_members_from_profiles(new_protected_branch.allowed_to_merge)
        self._update_members_from_profiles(new_protected_branch.allowed_to_push)
        if "allow_force_push" not in protected_branch:
            new_protected_branch["allow_force_push"] = None
        if "code_owner_approval_required" not in protected_branch:
            new_protected_branch["code_owner_approval_required"] = None
        return new_protected_branch

    def _update_members_from_profiles(self, allowed_action):
        if "profiles" in allowed_action:
            merge_profiles = self.get_merged_profiles(allowed_action.get("profiles"))
            members = allowed_action.get("members", [])
            allowed_action["members"] = list(set(merge_profiles + members))
            del allowed_action["profiles"]

    def init_protected_refs_auth(
        self, protected_branch_config, future_members_user, future_members_group
    ):
        if isinstance(protected_branch_config, str):
            return ProtectedRefsAuth(
                role=ProtectedRefMember(
                    self._get_role_id(protected_branch_config), protected_branch_config
                )
            )
        users = []
        groups = []
        role = None
        if "role" in protected_branch_config:
            role = ProtectedRefMember(
                self._get_role_id(protected_branch_config.role),
                protected_branch_config.role,
            )
        if "members" in protected_branch_config:
            self._init_members(protected_branch_config.members, users, groups)
            self._check_members(users, groups, future_members_user, future_members_group)
        return ProtectedRefsAuth(role=role, users=users, groups=groups)

    def _init_members(self, members, gpc_users, gpc_groups):
        for member_name in members:
            member = self._find_member(member_name)
            if isinstance(member, GPCUser):
                gpc_users.append(ProtectedRefMember(member.gl_id, member_name))
            else:
                # GPCGroup
                gpc_groups.append(ProtectedRefMember(member.gl_id, member.full_path))

    def _check_members(self, users, groups, future_members_user, future_members_group):
        unauthorize_members = self._get_unauthorize_users(
            users, future_members_user
        ) + self._get_unauthorize_groups(groups, future_members_group)
        if unauthorize_members:
            raise GpcMemberError(
                "Impossible to configure protected branches on project '{}' "
                "because these users and groups defined {}"
                " are not members of project".format(self.project_path, unauthorize_members)
            )

    def _get_unauthorize_users(self, users, future_members_user):
        members_id = self.members_id + future_members_user
        unauthorize_users = []
        for user in users:
            if user.member_id not in members_id:
                unauthorize_users.append(user.name)
        return unauthorize_users

    def _get_unauthorize_groups(self, groups, future_members_group):
        unauthorize_groups = []
        members_group = self.members_group + future_members_group
        for group in groups:
            if group.name not in members_group:
                unauthorize_groups.append(group.name)
        return unauthorize_groups
