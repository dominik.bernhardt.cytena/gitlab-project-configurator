"""
Mixin to extract label profiles from label section.
"""


# Third Party Libraries
from structlog import get_logger

# Gitlab-Project-Configurator Modules
from gpc.helpers.exceptions import GpcLabelError


log = get_logger()


class ProfileLabelMixin:
    def get_label_profile(self, profile_name):
        if not self.rule.get("label_profiles"):
            raise GpcLabelError(
                "ERROR on project {}: "
                "The import of label_profiles profile {} is impossible, because"
                "the section 'label_profiles' does not exist.".format(
                    self.project_path, profile_name
                )
            )
        for label_profile in self.rule.get("label_profiles"):
            if label_profile.name == profile_name:
                return label_profile
        raise GpcLabelError(
            "ERROR on project {}: "
            "The import of label profile {} is impossible, because"
            "this profile name is not found in the 'label_profiles' "
            "section.".format(self.project_path, profile_name)
        )
